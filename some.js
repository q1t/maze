
var context = document.getElementById('maze').getContext('2d');


const TileMap = {
	Rows: 80,
  Columns: 80,
  TSize: 16,
  ImageAtlas: _tileImage(),
  LogicGrid: [],
  VisualGrid: [],
};

const WALL = "WALL";
const PASSAGE = "PATH";
const FRONTIER = "FRONTIER";
const tileDict = {
	[WALL] : {r: 4, c: 4},
  [PASSAGE]: {r: 4, c: 3},
  [FRONTIER]: {r: 1, c: 8},
}

function genInitialMazeState() {
  let size = TileMap.Rows * TileMap.Columns;
  let state = [];
  while (size--) {
    state.push(WALL);
  };
  return state;
};

function genInitialMaze() {
  let state = genInitialMazeState();
  TileMap.LogicGrid = state;
  TileMap.VisualGrid = state;

  // load image?
  return true;
}

// supports only square grids, fixit
function drawGrid() {
	let height = TileMap.Columns * TileMap.TSize;
  let width = TileMap.Rows * TileMap.TSize;
  context.beginPath()
  context.moveTo(0,0);
  for (let c = 0; c <= TileMap.Columns; c++) {
  	context.moveTo(c * TileMap.TSize, 0);
  	context.lineTo(c * TileMap.TSize, height);
  }
  for (let r = 0; r <= TileMap.Rows; r++) {
  	context.moveTo(0, r * TileMap.TSize);
  	context.lineTo(width, r * TileMap.TSize);
  }
  context.closePath();
  context.stroke();
}
function clearCanvas() {
	context.clearRect(0,0, TileMap.Width, TileMap.Height);
}

function drawTile(tile, x, y) {
	if(tileDict.hasOwnProperty(tile)){
  	let d = tileDict[tile];
    let sx = TileMap.TSize * (d.c - 1);
    let sy = TileMap.TSize * (d.r - 1);
    context.drawImage(
    	TileMap.ImageAtlas,
      sx,
      sy,
      TileMap.TSize,
      TileMap.TSize,
      x,
      y,
      TileMap.TSize,
      TileMap.TSize
    )
  }
};
function drawTiles(state) {
	for(let c = 0; c < TileMap.Columns; c++){
  	for(let r = 0; r < TileMap.Rows; r++) {
    	let tile = state[r * TileMap.Columns + c];
      drawTile(tile, r*TileMap.TSize , c*TileMap.TSize);
    }
  }
}
var initState = genInitialMazeState();
drawTiles(initState);
let Maze = {
	Grid: [],
	Frontiers: [],
}
// drawGrid();

// Generate Maze state by state
const StartCell = 0;
function genMaze() {
	Maze.Grid = genInitialMazeState();
  Maze.Grid[StartCell] = PASSAGE;
  
  computeAdjIndexes(StartCell, 2).filter(function(i) {
  	return Maze.Grid[i] == WALL;
  }).map(function(frontier) {
  	Maze.Frontiers.push(frontier)
  })
  console.log(Maze);
  while (Maze.Frontiers.length > 0) {
  	let len = Maze.Frontiers.length;
    let rnd_f = Maze.Frontiers[Math.floor(Math.random() * len)];
    let neighbors = computeAdjIndexes(rnd_f, 2)
    	.filter(function(i) { return i != WALL });
    let rnd_n = neighbors[Math.floor(Math.random() * neighbors.length)];
    Maze.Grid[(rnd_f + rnd_n) / 2] = PASSAGE;
    computeAdjIndexes(rnd_f, 2).filter(function(i) {
  		return Maze.Grid[i] == WALL;
  	}).map(function(frontier) {
  		Maze.Frontiers.push(frontier)
  	})
  }
}
genMaze();
// distance should be > 1
function computeAdjIndexes(x, d) {
  let row = Math.floor(x / TileMap.Columns);
	let h = [x + d, x - d];
  let v = [x - (d * TileMap.Columns), x + (d * TileMap.Columns)];
  
  h = h.filter(function(i) { 
    let min = row * TileMap.Columns;
    let max = (row + 1) * (TileMap.Columns - 1);
  	return i >= min && i <= max
  })
  
  return v.concat(h).filter(function(i) {	
  	return i >= 0 && i <= TileMap.Columns*TileMap.Rows 
  })
}


function _tileImage() {
	var r = new Image();
  r.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOvAAADrwBlbxySQAAABh0RVh0U29mdHdhcmUAcGFpbnQubmV0IDQuMC42/Ixj3wAAE59JREFUeF7tndvP5dVdxrcXtAxMGaB0puCEMm0ZerA6TUNtYscx2piYUsPAMByGgUlNzHA+M6SaaCKJiXcmxqTxBm+8MJpo/wEvqpU2KTckemWMRTowHEuBkaL+XJ/1vs/m2d93rd/ev3ePUrPWSp781voe3r3293nWYe998c4+94ezIeK3vzkbLvvrrfYxzPaXUYpdBr3+jn0fzCjFjEH5V1xxRUYpRnj1v4bhjYTvvjQMZ9LzpXc2ni//9zB858wwfD/ZTzNOeHXYeL6T8JOEt1PMWL7mv918zX8s/5d+51tb8PU/++7K9iwACuZF8XH0OeSb7U7P39+VcfU9OzN+5sQsI/tCnqD80utTuI8e/fBw0a9fWBXBWD6Fe/bZZ1PJNgrpfo+j2Kf/c6OYZzfxVgKF/ou/e3b4YSIEks6mmJcSXkx9yHo9PbHX8uP8p+bH+dfyI6HX3HgqP2sikF+YC4AVEwspX6nv8Xmlb5Iv4h0SAU/B83n6WMXbde2FGSpi7fVjvhdPDZsLQfEvs+JSYX9M8VNBX0v9l98dMvH/fjaRkcaQ9GZ6vppi6CMMViX9Un5t/qvmj80/5jvRkdySCDwG/8IR4AX2vtsonNsyuUY+YpgTT//oBojDRhzxOc/+DtBregF9FZVe34E/Fu+ZZ57Z7G00FwF4c5OUp89sFBaw1VLgp1/cWGnsBq8kYtiq8b+WBPJ8GoNSfm3+q+SvMn/PF5GRfNmjDRArX/EOAMaKLaIARMaVr/Nf5AuKGxOBikfRtIJUSHzE+OtHUCDaPffck4vHU301F8HriQC21OdSMX+UiMiEpwJjfyU9RSp2zl7w8k82dghWdMxfNv9l+avOX/lOqvqOaI/juQDGigpcDFEAWvUl0gVIV5xE4ALgb1KgWDTv+06gPM/34j355JMLeOqpp3IRT548uSAAViWFZ9WzstiKIeStZHsn9d9MNrboVxJpP0424tkRiPlhsnv+qvOv5U+Zv/JrxNbscVfIAtB5qKI4anYBEp382Zfeg8jHN7tu024ikAD0+l5AFUxjoALGOZBPUSgOhaJgFJH+0aNHF4oYBcDK/vbpjZXOZez5RDgXLcaQg58t+u0ExPF6isHH9svq9fxV51/Knzp/5TuZY1s+KPmLR4CTvkwYWQAiOhH82R2z4fA1O/MzE88Okcg/+fmLhi/sOW8uDF/9QMXTqvECRhuxcV4qIIWiaCrgwYMH57ZYRPJYRWy/+aNWKuiZtNIhBZJewLbZB2fStsul7c1kQxwIQ/lT5x/ztzN/8iOhkWQfFwVQIhhb6fz3WMXwlAgkAMgGmfwEkZ9FsWnzfC+eiibEQtJXEZWv4lEcikTRvK/Cya4Cks/2y807HwFJAK8mAfBRLH9ES7sBt/+88tOYbZcYyMvnePKRv935K3+78yc/EgpEdInwLXcAEeFE10BsyUa+iAUQDeESgsiv5ccC8iwV8IIDH5z7yVG+F1AFA6yiWED6KiD5fP5+wy57kAzxEJ13BISQgAgAt3Y+ETyXYr53ZuPz+3bnr/ztzp98J1PQll/z+XjpJXC74nARSADuj6AggCKNQXHK8+JRHBWJ4mnbxOZbKk8V0bdirXLAfYBb9r+mlf78ZgyfChABfY4HPiYqf+r8lb/u/J1MIILXFkBNEKDkK+VDOgKI5I/lq0A1eCxQASmUCkaxSgUEKrAK+Hba+iEDkl9I5ztk5i990pO+dgbuAnwy4BLIXSDfCdI45pfm7Ij5685/7IwfE4FQvATqbNXY+9FXgmJKAlg1f/9lHxqu3XtpBn33eT5FOHz48LxgKpQXy4uoopJDLt/1i2RWJES/kUjSRzXuBAjilbTyWe2vJZw+m8bpSU4tP86/lr/u/CEU4kvnPYgiiHFZAMsImeqHcJEvxF3AUfr7NQGUoCI6KJiPKZyPySGX1fuP6Sz9h0QeHwG18gFE8vGPrR8xEHv6PxLJaQXnu0Ma1/Lj/Mfytzt/8p3MmghkL/lncUXV+jWf51+w//xMNE9I5+l9+Wr5+ht6AhVQY8XFfAoyBcr/t0Qa5z/n/Y/Sauebur9/YeMO8GIi6wdvpX6y8/FQKx2C+QzORTHmM0fya/Ov5ZfmOAblO5m1LR/UxLHlCKAwFFV92Z2oSBrgzV36lV1z+Fh9f8Z8/xsxN45Lry9gnzJ/vlL9QSIAUrjgseJFNGf66QR2AF0E300xuixCnOevOv9a/nZePxKqsz3aQXEH+Js/umH45z//6hY891e3ZNTGgufPUkEdU/MdxErtGk/N9/ha/sEDHx2Of21/fo5hLOZc5u+4/PxReB6IhAIRrfpFu2MWCwLGCl3ygdnJRHpEEoH8y/IdxDBx/z18an601fJjQcHtx68u2oH7anHbyRfBHz921bD3uisydh/8yBzYL/ncxQtCID8SqtUf6xf9whYBqEC1QrtdRRXh7s+2U++JwHcG2ZTvIN8nr4YNlF4ffHnfh+f5esrmiPkiQGA1rkqg49LZLD89XzZHKV/Eg0j8lYf3ZuBjjAgkBHJ9VZfIV1P9XADkLgjAi1Mau73kwzZf/ZAvQLrshlJ+nLz/DEqTCPz1Ifqbhw8sEF6zAc+niGOEg2V+iP69X9y9QLjbXBAeI/jKF/GMIV4iuOrolUURiMga+aX6ESvhzFQMFSSuStlLY88Fc3IPbT4hP5CtfGwxHzsTpPGZl8nrM7G/EWLi60PsvksvmBNO/xu/es18HP3KhwAIWnaOj/nJh9iZEUz/2CfS5W9zHP1CJJ8+EPkSgIBPIpAAgAuAtqx+yhvdAaK95gciGvLzOD3/9Ou/8p4YNgmPeQ6fvL4AEfj6kzehb8BK+ZAr0o/+/M9mwnm6PeZAQg010ms7AuSK9APnbxDO0+3EeT4kOvkiOpLvY+LoRwFMqd8kAbArRF/cKZx8sHvXjuHaT+zJcBHIH/OZFJPTd+K8Cfr65ktvQm8g5gPmCMla/f6M5CtfRKyKZXcESNbq96fIj/kuACf6ov0f2gL5FOcCmFq/lQUgW00cQOR7DAJgBwD0SyJw6A0wUSatN6CvQ/VG/E3Ev6HXjyJgXJt/icwxgiNKsVEEIr8ESBSxYE72zvOHKy++cDj08T25T5yLgDH5UQBT6jcXgK8mL1QsWq2IvvIVs//yi4ffOPCxDASguJIANHkmxySZtPc1cdmjAErzh/QTX/jYwuovzR8C41Y/dt5H1PIh/cu7NoQwJgDyJQIIpp/HiXTIRwT0iZUIRD5w8qfWb8sOACiSEH3yl+wCfsjX6geM5XexCP4GNGGAiuMboB8F4NDcfReQAOT3eBWyhrjCSyu+BN8FXAC1fBFPX6tfIpAAFKc+iAKYUr8tAojFieNV7RCu8x+4ADwO+OSZnCbJ5LVtYfMtjWdJBP53XQDsBDURqJA1YrD7Cve4mh24ANgJJIJV8scEEPPXrV/+GFgqjI/jhauWoz6k+w4QdwHP0Rtgopowky29AaA3qDdQen2RryNgFQGooD5ehki68kW+jgAXgKOUL/JFfDwGHOSvW7+8A1AQL4qjZI/xcYwAdP4L2ErxTIKfNzVhTdQn629Cb0o/ifrf+63rdi2Qz+rXRVAiIMZfPxZ1DMsEcsmODyyQz+rXRVAiIKaUCyL5Il7P0uuvW7+FH4NUFEe0+Zh+zKfApR0Am4of8/UmHEzYx0zcx+T46/O3p0CvH1ehY8wnKAZipyDmA0gW+UIUAVA80B3AawNWqd/SO0C0uz8eC6BU6BJK+UxoCkqvv535+6pyMrDXBBDjavYp+YjCyfcjQE+JwAUEiRLBFCiv+ClgSnEj/r/lq5CgRhgY8wnbzfedYQrIFZEC3wjqa+GI4s/Bh/btHk5+8ZNz7LvkwoWxQz7P+d/Ml1qjz3M4X9fFpz5y0XDswFW5qDxX6ZND7n1f/YXh2C9/avjdo1/M/ak48Wuf2YJrr95TtIPo+6fLZ1tw5rNbbTX7ggDoOwGMxwgC8p933nlb8mXTGNTy1Zcf4v33bMXEfIhYB1EAXBRrpHuMckukjgGhuFicTKEmgJI9Evq3OzaeNRHIL8woopOgAkfihJIfoi/b+YH8lN9tHqdxJF598p18Ne0G8fVFjAOChDE//SkCwLdMAOwGINoF+SWCSCiYIgAnOpJbEoHH4M8C8OLrKbgv+uWD1MdOXLMgAvpuA/Q9RvkCtki+/4xJkwiUDyHrYEwAUQw1AUBmJN7HcdU7IqE18oH7HvzNz2eIyEi+7NEGiJVvLgAVNK58L3bJx1Pk+hMc+cqeLXY9Pd/7EEzjM+vY79nKETEObCWUYs6VAGoEy1eKQSAiUhgTAMCvOOCkqu+I9jjOAnAiIsnA/Q7F4he5Iv3nrtyRbTzdLvJjPk8nX19gCHx96b9nKx9i1oEEIHJlLwnA7S6AdSAihSgIJ9+Jl61GbM0ed4WZij8mgjEBeL6vcAj3ZyQ/5mdSE7n6ThsR0Nc3VxJBFACErINVBRDFcK4E4AQDFwNwMcjm8U7m2JYPSv65AECJaCcYSBx6xvySCDQey5cAIFpfaUK4vs6UEKIIRBiAJIf7SjGMxwTgfcWXBKDzvrTNC4oBbncyBSdZpMs2JgAQSfZxUQAU0REJFVFu85gIfCL9S5/euSAA+WN+XP2Q7n0RL3tNADU44RESAP3tCgDUznkg0uV3ETiZQiTZsUwAQESXCC/eAQRflZF494OSCOR30v3sr+W7AEQ4YBeIAqDvAoCQdbBMALWdIQpgClwkTqZQE0DJ7mQK2vJrPh/nIwBiIuHYI8m1mJgfBcBO4LuA55dWv8jXto/NjwTfBURMhFZ9beUL74cAHJFQnfXRDlYRgAieJAAnRcTwhFTvl2JivpOvI6AkAOVJABAtwiG7JAAQdwERswrhHqO4KADZl4mhJoDSOV+CjgIR6Rc9J7jk5yn72Bk/JgJh/jGwRmjJVhqDyy55j2hIZ/XrIigREOP5kMjPkyJcRDvZLgKJQj9pQsg6GBNAFMMyATjx9Ev3gQgnVpDN4X6PgVCIL533IIogxmUBaHVHYh0iWTG+I9DP5E+A50sEDgj3McT7OO8cKVfERGCvwWMkAMarCoBnbQcQdOFbJgIR6qvdiY5+9WV3MmsikL3kX7gEugAgyEmSrSQAYZ18CJ0C5UPEOogCgOhzIYAadERIGCLW4YRHRJ+TWdvyQU0ccwGooE6i2x0xBrxf+ZCwLiDTiV6lHwVQ2/L9WBB8Z3Ayhe0KAOhsj3ZQ3AGe/uO9w19+Y+/wL0/tHehPxbf+YCvuP1K2g+h75JFHhrvvvnt49NFHh2PHjg133nnncP/99w8PP/zwcPPNNw+PPfbYcN999+W422+/Pfdvu+22HANKu8MUtN6yAKYAobhYnEyhJoCSHaKfeOKJ4dZbb80kM+b5+OOP5yf2hx56KAuBs58n4xtuuCH7IfHQoUNFIJgxP2i9bREAuwGIdkF+iSASCqYIgFV86tSpTNaDDz6YwW4AWOmQDdHsDvgQBr4jR450AZyDlgUAmZF4H8dV79hCaIV84L7v/ckGbrnllnzDh1S2fAhm1WNnN8AH2Q888EAmXDsEoiEOgiG6hmX+1ttcADWC5SvFIBARKYwJAOBXHIB4VrmOAojnToANMQBWvh8RN9100/weAMHroPU2+Q4QISKFKAgn34mXDVIh+N57782ksyohnT5AENiIo48QuAfouPDtnDhIdZuj5G+9rS0AJxi4GICLQTaPh5Qbb7wxk63bPqubVa5jAB87BYTr7EckCMHJ7AKY3rIAdN6XtnlBMcDtTqbgJIt02aIAdKYjAla37gNA9wH8iINYxtwVEAOxkOqA4GhzRH/rbb4D1M55INLldxE4mUIk2RF9XO5Y4TyPHz+eSYdwbJzx7AL0+RQAOCpY+RIBhK6Dn6Z29p13N3v/d21bR4CLxMkUagIo2SGeH3nYASD3rrvumgtAuwMrXbsAt3+OCC6M+CHRt3SHVnzJJ7TezvkdQGd9tIOSndUMoax+vgWEaFY6T3YAtnxWOmQzRhha/eR2AazXtgigdM6XoKNARPpFzwku+XnKzrd7J06cyARDKFs+5/8dd9wx70M4giCGHYNjAhuigWA/0yOW+VtvCwJw4umX7gMRTqwgm8P9HgOpkMwuoFWvL30APohixWNn62eMHT8Er4PWW/UI0IVvmQhEqK92Jzr61ZcdUtnW2f4Rg1Y6uwFPhMFqB8RxJLDyJQ7fzhEGpLrNUfK33ibfAXRESBgi1uGER0Qf5z3kahfQrR9h8JkfIXDx4wKoH4CALoZOZhfA9DYXQG3L92NB8J3ByRSmCICtHcIhmk8BrPrrr78+kw/xkCxBIALuC/Q5KrgnQKoDgqPNEf2tt9nms7dGWxdA460LoPHWBdB46wJovHUBNN66ABpvXQCNty6AxlsXQOOtC6Dx1gXQeOsCaLx1ATTeugAab10AjbcugMZbF0DjrQug8dYF0HjrAmi8dQE03roAGm9dAI23LoDGWxdA460LoPHWBdB46wJovHUBNN66ABpvXQCNty6AxlsXQOOtC6Dx1gXQeOsCaLx1ATTeugAab10AjbcugMZbF0DjrQug8dYF0HjrAmi8dQE03roAGm9dAI23LoDGWxdA460L4KeovS//OXTz2VujrQug8dYF0HjrAmi8dQE03roAGm9dAI23LoDGWxdA460LoPHWBdB46wJovHUBNN2G4X8AoegxHmCvd/EAAAAASUVORK5CYII=";
  return r;
}
