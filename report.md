# Report 

Analysis of running time of modified Prim's algorithm to generate mazes:
This version of an algorithm is faster then original one(but perhaps not with my implementation) because it does not need to maintain a list of edges alongside with adjacency list or matrix, but only a list of "frontiers" (next vertices to inspect), while the whole graph (2D grid) could be maintained in an array. 

Since we do not have an adjacency list, we have a function to find all adjacent "cells" of a cell which runs in O(1) ("computeAdjIndexes" in source code) which calculates 4 integers and checks them to be in boundaries of the array.

For storing vertices and frontiers we use arrays with O(1) for indexing and O(1) for inserting at the end (o(n) worst case),
while deleting and shifting its indexes for an array is O(n)

Running time for the whole algorithm is O(Vlog(V)) * O(V) (deletion on of V vertecies from array and shifting it) which is O((V^2)*log(V)) (not sure, #fixit)

Animation code also works poorly - rerendering whole tilemap every animation tick over and over even if some tiles remain unchanged.

---

*Data Sctructures and Algorithms Class* helped me alot
in understanding of fundamental knowledge of CS and computation in general; 
_And also showed me that some graph problems could be fun._
